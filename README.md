## Used technologies:

- Spring Boot
- Spring Data (JPA, REST-WEBMVC, )
- Hikari 
- Junit
- AssertJ
- Spring Test DBUnit
- QueryDSL


## Entities

There are just two entities: `Employee` and `Address`. 

#### `Employee`
- id
- firstName     
- lastName
- description
- address

#### `Address`
- id 
- street
- country
- number

Change the port of the DB in the `application.properties` file when switching from Windows to OS X.
