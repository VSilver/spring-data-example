package com.endava.internship.repository;

import com.endava.internship.entities.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>,
                                            PagingAndSortingRepository<Employee, Long>,
                                            QueryDslPredicateExecutor<Employee>,
                                            EmployeeRepositoryCustom {

    List<Employee> findByFirstName(String firstName);

    List<Employee> findByLastName(String lastName);

    List<Employee> findByFirstNameAndLastName(String firstName, String lastName);

    List<Employee> findByDescription(String description);

    List<Employee> findByDescriptionOrderByLastNameDesc(String description);

    @Transactional
    void deleteByFirstName(String firstName);

    Employee findFirstByLastName(String lastName);

    List<Employee> findByAddressCountry(String country);

    // Using simple JPQL
    @Query("select e from Employee e where e.id = ?1")
    Employee findOneById(Long id);

    @Query("select e from Employee e where e.description like %?1")
    List<Employee> findAllByDescriptionLike(String description);

    // Using Native Query
    @Query(value = "SELECT * FROM EMPLOYEE WHERE FIRSTNAME = ?1", nativeQuery = true)
    List<Employee> findAllByFirstNameNativeQuery(String firstName);

    // Using JPQL with aggregations
    @Query("select e from Employee e join fetch e.address a where a.country = :country")
    List<Employee> findAllByCountry(@Param("country") String country);

    // Using SpEL expressions
    @Query("select e from #{#entityName} e join fetch e.address a where a.street = ?1")
    List<Employee> findAllByStreet(String street);

    Page<Employee> findAll(Pageable pageable);
}

