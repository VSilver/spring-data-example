package com.endava.internship.repository;


import com.endava.internship.entities.Employee;
import com.endava.internship.util.EmployeeRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class EmployeeRepositoryImpl implements EmployeeRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Employee> searchEmployeesByFirstName(String firstName, int limit) {
        List<Object> params = new ArrayList<>();

        String queryString = "select * from Employee e where e.firstName = ?0 limit ?1";
        params.add(firstName);
        params.add(limit);

        Query query = entityManager.createNativeQuery(queryString, Employee.class);
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }

        List<Employee> result = query.getResultList();

        return result;
    }

    @Override
    public List<Employee> searchEmployeesByLastName(String lastName, int limit) {
        String queryString = "select * from Employee e join Address a on (e.address_id=a.id) where e.lastName = '" +
                lastName + "' limit " + limit;

//        List<Employee> result = jdbcTemplate.query(queryString, new EmployeeRowMapper());
        List<Employee> result = jdbcTemplate.queryForList(queryString, Employee.class);

        return result;
    }
}
