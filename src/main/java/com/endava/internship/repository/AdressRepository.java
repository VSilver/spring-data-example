package com.endava.internship.repository;


import com.endava.internship.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AdressRepository extends JpaRepository<Address, Long>,
                                            PagingAndSortingRepository<Address, Long>,
                                            QueryDslPredicateExecutor<Address> {

}
