package com.endava.internship.repository;


import com.endava.internship.entities.Employee;

import java.util.List;


public interface EmployeeRepositoryCustom {

    List<Employee> searchEmployeesByFirstName(String firstName, int limit);

    List<Employee> searchEmployeesByLastName(String lastName, int limit);
}
