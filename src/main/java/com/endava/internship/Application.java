package com.endava.internship;

import com.endava.internship.config.JpaConfig;
import com.endava.internship.entities.Employee;
import com.endava.internship.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Application {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private EmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[]{Application.class, JpaConfig.class}, args);
    }

    @Bean
    public CommandLineRunner demo(EmployeeRepository employeeRepository) {
        return (args) -> {
            System.out.println("From main");
            Employee emp = new Employee();
            employeeRepository.save(emp);
            emp.setLastName("Vrabie");
            emp.setFirstName("Adrian");
            employeeRepository.save(emp);
            System.out.println(emp);

            Employee emp2 = new Employee();
            employeeRepository.save(emp2);
            emp2.setLastName("Vrabie");
            emp2.setFirstName("Gabriel");
            employeeRepository.save(emp2);
            System.out.println(emp2);

            employeeRepository.findByLastName("Vrabie").forEach(System.out::println);
            System.out.println(employeeRepository.findByLastName("Vrabie").size());
        };
    }
}
