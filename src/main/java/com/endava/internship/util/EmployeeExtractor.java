package com.endava.internship.util;


import com.endava.internship.entities.Address;
import com.endava.internship.entities.Employee;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class EmployeeExtractor implements ResultSetExtractor<List<Employee>> {
    @Override
    public List<Employee> extractData(ResultSet resultSet) throws SQLException, DataAccessException {

        Map<Long, Employee> employeeMap = new HashMap<>();
        Map<Long, Address> addressMap = new HashMap<>();

        Set<Address> addressSet = null;

        while (resultSet.next()) {
            System.out.println("-------------------------------------");
            System.out.println("Has next");
            Long id = resultSet.getLong("id");
            Employee employee = employeeMap.get(id);

            if (employee == null) {
                employee = new Employee();
                employee.setId(id);
                employee.setDescription(resultSet.getString("description"));
                employee.setFirstName(resultSet.getString("firstName"));
                employee.setLastName(resultSet.getString("lastName"));
                employeeMap.put(id, employee);
            }

            Long address_id = resultSet.getLong("address_id");
            Address address = addressMap.get(address_id);

            if (address == null) {
                address = new Address();
                address.setId(address_id);
                address.setCountry(resultSet.getString("addresscountry"));
                address.setStreet(resultSet.getString("addressstreet"));
                address.setNumber(resultSet.getInt("addressnumber"));
                addressMap.put(address_id, address);
            }

            employee.setAddress(address);
        }

        return new ArrayList<>(employeeMap.values());
    }
}
