package com.endava.internship.util;


import com.endava.internship.entities.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeRowMapper implements RowMapper<Employee> {

    @Override
    public Employee mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
        Employee employee = new Employee();

        employee.setId(resultSet.getLong("id"));
        employee.setLastName(resultSet.getString("lastName"));
        employee.setFirstName(resultSet.getString("firstName"));
        employee.setDescription(resultSet.getString("description"));
//        employee.setAddress(resultSet.getObject("address"));

        return employee;
    }
}
