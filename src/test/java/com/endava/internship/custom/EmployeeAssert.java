package com.endava.internship.custom;

import com.endava.internship.entities.Employee;
import org.assertj.core.api.AbstractAssert;


public class EmployeeAssert extends AbstractAssert<EmployeeAssert, Employee> {

    public EmployeeAssert(Employee actual) {
        super(actual, EmployeeAssert.class);
    }

    public static EmployeeAssert assertThat(Employee actual) {
        return new EmployeeAssert(actual);
    }

    public EmployeeAssert hasLastName(String lastName) {
        isNotNull();

        if(!actual.getLastName().equals(lastName)) {
            failWithMessage("Expected employee's last name to be <%s> but was <%s>", lastName, actual.getLastName());
        }

        return this;
    }

    public EmployeeAssert hasFirstName(String firstName) {
        isNotNull();

        if(!actual.getFirstName().equals(firstName)) {
            failWithMessage("Expected employee's last name to be <%s> but was <%s>", firstName, actual.getFirstName());
        }

        return this;
    }

    public EmployeeAssert hasDescription(String description) {
        isNotNull();

        if(!actual.getDescription().equals(description)) {
            failWithMessage("Expected employee's descritpion to be <%s> but was <%s>", description, actual.getDescription());
        }

        return this;
    }

}
