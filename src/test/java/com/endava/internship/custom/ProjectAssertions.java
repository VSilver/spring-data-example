package com.endava.internship.custom;


import com.endava.internship.entities.Address;
import com.endava.internship.entities.Employee;
import org.assertj.core.api.Assertions;


public class ProjectAssertions extends Assertions {

    public static EmployeeAssert assertThat(Employee actual) {
        return new EmployeeAssert(actual);
    }

    public static AdressAssert assertThat(Address actual) {
        return new AdressAssert(actual);
    }
}
