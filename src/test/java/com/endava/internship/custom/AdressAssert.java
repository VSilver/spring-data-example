package com.endava.internship.custom;


import com.endava.internship.entities.Address;
import org.assertj.core.api.AbstractAssert;

public class AdressAssert extends AbstractAssert<AdressAssert, Address> {

    public AdressAssert(Address actual) {
        super(actual, AdressAssert.class);
    }

    public static AdressAssert assertThat(Address actual) {
        return new AdressAssert(actual);
    }

    public AdressAssert hasStreet(String street) {
        isNotNull();

        if(!actual.getStreet().equals(street)) {
            failWithMessage("Expected employee's last name to be <%s> but was <%s>", street, actual.getStreet());
        }

        return this;
    }

    public AdressAssert hasCountry(String country) {
        isNotNull();

        if(!actual.getCountry().equals(country)) {
            failWithMessage("Expected employee's last name to be <%s> but was <%s>", country, actual.getCountry());
        }

        return this;
    }

    public AdressAssert hasNumber(Integer number) {
        isNotNull();

        if(!actual.getNumber().equals(number)) {
            failWithMessage("Expected employee's last name to be <%s> but was <%s>", number, actual.getNumber());
        }

        return this;
    }
}
