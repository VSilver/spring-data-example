package com.endava.internship.repository;

import com.endava.internship.Application;
import com.endava.internship.config.JpaConfig;
import com.endava.internship.entities.Employee;
import com.endava.internship.entities.QEmployee;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.PageRequest;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.endava.internship.custom.ProjectAssertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, JpaConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection = {"configureDataSource"})
@DatabaseSetup(EmployeeRepositoryIT.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = {EmployeeRepositoryIT.DATASET})
public class EmployeeRepositoryIT {

    protected static final String DATASET = "classpath:datasets/employee-repository-test-entries.xml";
    protected static final int TOTAL_NUMBER_OF_EMPLOYEES = 5;
    protected static final int NUMBER_OF_EMPLOYEES_PER_PAGE = 3;

    @Autowired
    private EmployeeRepository repository;

    @Autowired
    LocalContainerEntityManagerFactoryBean entityManagerFactory;

    @Test
    public void findLastName_ReturnsTwoEmployees() {
        assertThat(repository.findByLastName("Vrabie"))
                .hasSize(2)
                .extracting("firstName")
                .containsOnly("Adrian", "Gabriel")
                .doesNotContain("Silvia");
    }

    @Test
    public void findAll_ReturnsTwoEmployees() {
        assertThat(repository.findAll())
                .hasSize(TOTAL_NUMBER_OF_EMPLOYEES);
    }

    @Test
    public void findByDescriptionOrderByLastNameDesc_ContainsOrderedEmployees() {
        assertThat(repository.findByDescriptionOrderByLastNameDesc("Custom description"))
                .extracting("lastName")
                .containsSequence("Rudicov", "Cadea");
    }

    @Test
    public void findByDescription_ReturnsTwoEmployees() {
        assertThat(repository.findByDescription("Some description"))
                .hasSize(2)
                .extracting("firstName")
                .containsOnly("Adrian", "Gabriel");
    }

    @Test
    public void deleteByFirstName_LeavesFourEmployees() {
        assertThat(repository.findAll())
                .hasSize(TOTAL_NUMBER_OF_EMPLOYEES);

        repository.deleteByFirstName("Adrian");
        
        assertThat(repository.findAll())
                .hasSize(TOTAL_NUMBER_OF_EMPLOYEES - 1);
    }

    @Test
    public void findFirstByLastName_ReturnsOneEmployee() {
        assertThat(repository.findFirstByLastName("Ladan"))
                .isNotNull()
                .hasFirstName("Vlad");
    }

    @Test
    public void findAll_GivenAPredicate_ReturnsTwoEmployee() {
        QEmployee employee = QEmployee.employee;

        Predicate predicate = employee.firstName.equalsIgnoreCase("dmitrii")
                .and(employee.lastName.startsWithIgnoreCase("cad"));

        System.out.println(repository.findAll(predicate));

        assertThat(repository.findAll(predicate))
                .hasSize(1)
                .extracting("firstName", "lastName")
                .contains(tuple("Dmitrii", "Cadea"));
    }

    @Test
    public void findAll_UseJPAQuery_ReturnsOneEmployee() {
        JPAQuery query = new JPAQuery(entityManagerFactory.nativeEntityManagerFactory.createEntityManager());

        QEmployee qEmployee = QEmployee.employee;

        assertThat(query
                .from(qEmployee)
                .where(qEmployee.firstName.equalsIgnoreCase("dmitrii")
                        .and(qEmployee.lastName.startsWithIgnoreCase("cad")))
                .list(qEmployee))
                .hasSize(1)
                .extracting("firstName", "lastName")
                .contains(tuple("Dmitrii", "Cadea"));
    }

    @Test
    public void findAllById_ReturnsOneEmployee() {
        assertThat(repository.findOneById(1L))
                .isNotNull()
                .hasFirstName("Adrian")
                .hasLastName("Vrabie");
    }

    @Test
    public void findAllByDescriptionLike_ReturnsAllButOneEmployees() {
        assertThat(repository.findAllByDescriptionLike("description"))
                .hasSize(TOTAL_NUMBER_OF_EMPLOYEES - 1)
                .extracting("firstName")
                .contains("Adrian", "Gabriel", "Dmitrii", "Silvia")
                .doesNotContain("Vlad");
    }

    @Test
    public void findAllByFirstNameNativeQuery_ReturnsOneEmployee() {
        assertThat(repository.findAllByFirstNameNativeQuery("Silvia"))
                .isNotNull()
                .hasSize(1)
                .extracting("lastName")
                .contains("Rudicov");
    }

    @Test
    public void findAllByCountry_ReturnsOneEmployee() {
        assertThat(repository.findAllByCountry("Moldova"))
                .hasSize(1)
                .extracting("firstName")
                .contains("Vlad");
    }

    @Test
    public void findByAddressCountry_ReturnsOneEmployee() {
        assertThat(repository.findByAddressCountry("Moldova"))
                .hasSize(1)
                .extracting("firstName")
                .contains("Vlad");
    }

    @Test
    public void findAllByStreet_ReturnsOneEmployee() {
        assertThat(repository.findAllByStreet("Stefan cel Mare"))
                .hasSize(1)
                .extracting("firstName")
                .contains("Vlad");
    }

    @Transactional
    @Test
    @Ignore
    public void searchEmployeeByFirstName_ReturnsOneEmployee() {
        Employee emp = new Employee("Ion", "Turcanu", "");
        repository.save(emp);
        repository.flush();

        List<Employee> result = repository.searchEmployeesByFirstName("", 2);
        assertThat(result)
                .hasSize(1)
                .extracting("lastName")
                .contains("Turcanu");
    }

//    @Transactional
    @Test
//    @Ignore
    public void insertEmployeeAndSearchEmployeeByLastName_ReturnsOneEmployee() {
        Employee emp = new Employee("Ion", "Turcanu", "");
        repository.saveAndFlush(emp);

        List<Employee> result = repository.searchEmployeesByLastName("Turcanu", 2);
        assertThat(result)
                .hasSize(1)
                .extracting("firstName")
                .contains("Ion");
    }

    @Test
    public void findAll_GivenPageableArgument_ReturnsSpecifiedAmountOfEmployees() {
        assertThat(repository.findAll(new PageRequest(0, NUMBER_OF_EMPLOYEES_PER_PAGE)))
                .hasSize(NUMBER_OF_EMPLOYEES_PER_PAGE);
        assertThat(repository.findAll(new PageRequest(1, NUMBER_OF_EMPLOYEES_PER_PAGE)))
                .hasSize(TOTAL_NUMBER_OF_EMPLOYEES - NUMBER_OF_EMPLOYEES_PER_PAGE);
    }
}
